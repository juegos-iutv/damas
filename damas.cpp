#include <iostream>
#include <stdlib.h>
using namespace std;
int main(){    
    
    bool t=false, r, e, mp=false;
    int f, c, b=0, n=0, x, a;     
    char d[8][8], l, q, m, v1[12], v2[12];
    
    // creacion de la matriz
    for (f=0; f<8; f++){
        for (c=0; c<8; c++){
            if (f==0 || f==2){ 
                              if (c%2==0){ d[f][c]=1; } // NEGRAS
                              else{ d[f][c]=' '; }
            }
            if (f==1){
                      if (c%2==0){ d[f][c]=' '; }
                      else{ d[f][c]=1; }
            }
                       
            if (f==5 || f==7){
                              if (c%2==0){ d[f][c]=' '; } // BLANCAS
                              else{ d[f][c]=2; }
                      
            }
            if (f==6){
                      if (c%2==0){ d[f][c]=2; } 
                      else{ d[f][c]=' '; }
            }
                                            
            if (f==3 || f==4){ d[f][c]=' '; }
        }
    }
    
    // creacion del vector (fichas comidas)
    for (int i=0; i<12; i++){ v1[i]='.'; }
    for (int i=0; i<12; i++){ v2[i]='.'; }
    
do{    
    // cambio de turno
    if (t==false){ t=true; }
    else{ t=false; }
    e=true;  
    
    // dibujar tablero
    system ("cls");
    cout << " o-------------------------------o" << endl;   
    
    for (f=0; f<8; f++){
        for (c=0; c<8; c++){
            cout << " | " << d[f][c];
            if (c==7){ cout << " | " << f; }
        }
        if (f<7){
           l=195; 
           cout << endl << " " << l << "-------------------------------"; l=180; cout << l;
           if (f==0 && mp==true){ cout << "               [modo prueba]"; }          
           if (f==1){ cout << "           JUEGO DE DAMAS EN C++ "; }
           if (f==4){ cout << "         ";
                      for (int i=0; i<12; i++){ cout << " " << v1[i]; }
           }           
           if (f==6){ cout << "         ";
                      for (int i=0; i<12; i++){ cout << " " << v2[i]; }
           }                      
           cout << endl;
        }        
    }
    cout << endl << " o-------------------------------o" << endl << "   0   1   2   3   4   5   6   7" << endl; 
    
    // determinar turno
    if (t==true){ l=2; q='B';
                  cout << endl << " JUGADOR 1 - ficha (" << l << " - " << q << ")" << endl; //BLANCA
    } 
       
    else{ l=1; q='N'; 
          cout << endl << " JUGADOR 2 - ficha (" << l << " - "<< q << ")" << endl; // NEGRA
    }
    
    // validar posicion de la ficha elegida (dentro del tablero)
    cout << endl << " # elija la ficha" << endl;    
    cout << " fila: ";
    cin >> f;
    if (f<0 || f>7){ cout << " *** ESA FILA NO EXISTE ***" << endl; 
                     if (t==false){ t=true; }
                     else{ t=false; }
    }
                
    else{   
         cout << " columna: ";
         cin >> c;
         if (c<0 || c>7){ cout << " *** ESA COLUMNA NO EXISTE ***" << endl; 
                          if (t==false){ t=true; }
                          else{ t=false; }
                          e=false;
         }
    }
    
    // si elige una posicion vacia
    if (d[f][c]==' ' && f<=7 && c<=7){ cout << " *** NO HAY FICHA QUE MOVER ***" << endl;
                       if (t==false){ t=true; }
                       else{ t=false; }
    }     
    
    // si elige ficha del oponente       
    if(l==2){ // BLANCA
             if (d[f][c]==1 || d[f][c]=='N'){ cout << " *** DEJE LA TRAMPA! USTED NO PUEDE MOVER ESA FICHA ***" << endl; 
                                              if (t==false){ t=true; }
                                              else{ t=false; }
             }
    }    
    if(l==1){ // NEGRA
             if (d[f][c]==2 || d[f][c]=='B'){ cout << " *** DEJE LA TRAMPA! USTED NO PUEDE MOVER ESA FICHA ***" << endl; 
                                              if (t==false){ t=true; }
                                              else{ t=false; }
             }
    }
    
    // si elige peon    
    if (d[f][c]==l && e==true){
                    cout << endl << " # hacia que lado movera el peon? use < o > : ";
                    cin >> m;
                    
                    switch (m){
                           case '>':
                                if (c==7){ 
                                          cout << " *** NO PUEDES SALIR DEL TABLERO ***" << endl;
                                          if (t==false){ t=true; }
                                          else{ t=false; }
                                }                                
                                else{ // BLANCA vs negra
                                      if (l==2){ 
                                                if (d[f-1][c+1]==' '){ d[f][c]=' '; 
                                                                                d[f-1][c+1]=l;
                                                                                e=false; 
                                                }                                     
                                                if (d[f-1][c+1]==1 || d[f-1][c+1]=='N'){
                                                                   if (d[f-2][c+2]==' ' && c+2<8){ d[f][c]=' '; 
                                                                                                   d[f-1][c+1]=' ';
                                                                                                   v2[b]=1; b++;
                                                                                                   d[f-2][c+2]=l;
                                                                                                   e=false;
                                                                   } 
                                                }                                               
                                                if (d[f-1][c+1]!=' ' || d[f-2][c+2]!=' '){ 
                                                   if (e==true){ cout << " *** NO PUEDES COMER ESA FICHA ***" << endl;
                                                                 if (t==false){ t=true; }
                                                                 else{ t=false; }
                                                   }                                                 
                                                }                                                                                                                                                                                                                                                                                                                                                                                                                  
                                      }
                                      
                                      // NEGRA vs blanca                                              
                                      if (l==1){ 
                                                if (d[f+1][c+1]==' '){ d[f][c]=' ';
                                                                       d[f+1][c+1]=l;
                                                                       e=false; 
                                                }                         
                                                if (d[f+1][c+1]==2 || d[f+1][c+1]=='B'){
                                                                   if (d[f+2][c+2]==' ' && c+2<8){ d[f][c]=' ';
                                                                                                   d[f+1][c+1]=' ';
                                                                                                   v1[n]=2; n++;
                                                                                                   d[f+2][c+2]=l;
                                                                                                   e=false;
                                                                   } 
                                                }                                              
                                                if (d[f+1][c+1]!=' ' || d[f+2][c+2]!=' '){
                                                   if (e==true){ cout << " *** NO PUEDES COMER ESA FICHA ***" << endl;
                                                                 if (t==false){ t=true; }
                                                                 else{ t=false; }
                                                   }
                                                }
                                      }                                               
                                }                                                                                                       
                           break;
                    
                           case '<':                                 
                                if (c==0){ cout << " *** NO PUEDES SALIR DEL TABLERO ***" << endl;
                                           if (t==false){ t=true;}
                                           else{ t=false;} 
                                }                                
                                else{
                                     if (l==2){ // BLANCA vs negra
                                               if (d[f-1][c-1]==' '){ d[f][c]=' ';
                                                                      d[f-1][c-1]=l;
                                                                      e=false; 
                                               }                                     
                                               if (d[f-1][c-1]==1 || d[f-1][c-1]=='N'){
                                                                  if (d[f-2][c-2]==' ' && c-2>=0){ d[f][c]=' '; 
                                                                                                   d[f-1][c-1]=' ';
                                                                                                   v2[b]=1; b++; 
                                                                                                   d[f-2][c-2]=l;
                                                                                                   e=false;
                                                                  } 
                                               }                                                                
                                               if (d[f-1][c-1]!=' ' || d[f-2][c-2]!=' '){
                                                  if (e==true){ cout << " *** NO PUEDES COMER ESA FICHA ***" << endl;
                                                                if (t==false){ t=true; }
                                                                else{ t=false; }
                                                  }
                                               }                                                                                                                                                                                                                                                                                                      
                                     }
                                                                                   
                                     if (l==1){ // NEGRA vs blanca 
                                               if (d[f+1][c-1]==' '){ d[f][c]=' '; 
                                                                      d[f+1][c-1]=l;
                                                                      e=false; 
                                               }
                                               if (d[f+1][c-1]==2 || d[f+1][c-1]=='B'){
                                                                  if (d[f+2][c-2]==' ' && c-2>=0){ d[f][c]=' ';
                                                                                                 d[f+1][c-1]=' ';
                                                                                                 v1[n]=2; n++;
                                                                                                 d[f+2][c-2]=l;
                                                                                                 e=false;
                                                                  } 
                                               }
                                               
                                               if (d[f+1][c-1]!=' ' || d[f+2][c-2]!=' '){
                                                  if (e==true){ cout << " *** NO PUEDES COMER ESA FICHA ***" << endl;
                                                                if (t==false){ t=true; }
                                                                else{ t=false; }
                                                  }
                                               }                                                                                 
                                     }
                                }                                
                           break;
                                                    
                           default: cout << " *** MOVIMIENTO NO VALIDO use < o > ***" << endl;
                                    if (t==false){ t=true; }
                                    else{ t=false; }
                    }                      
    }
    
    // si elije reina 
    if (d[f][c]==q && e==true){                   
                   cout << endl << " # Hacia donde movera la reina?" << endl;
                   
                   cout << " arriba o abajo? use v o ^ : ";
                   cin >> m;
                   
                   cout << " hacia que columna?: ";
                   cin >> a;
                   if (a<0 || a>7){ cout << " *** ESA COLUMNA NO EXISTE ***" << endl; 
                                    if (t==false){ t=true; }
                                    else{ t=false; }
                                    e=false; 
                   }
                   
                   if (e==true){
                      switch (m){
                             case '^':
                                  if (c>a){ // izquierda
                                            x=c-a;
                                            if (d[f-x][c-x]==' '){ 
                                               e=false;                                                     
                                               for (int i=1; i<x ; i++){                                                   
                                                   if (q=='B'){ // BLANCAS vs negras
                                                                if (d[f-i][c-i]==1 || d[f-i][c-i]=='N'){ d[f-i][c-i]=' ';
                                                                                                         v2[b]=1; b++;
                                                                }
                                                                if (d[f-i][c-i]==2 || d[f-i][c-i]=='B'){ cout << " *** NO PUEDE COMER FICHAS BLANCAS ***" << endl;
                                                                                                        if (t==false){ t=true; }
                                                                                                        else{ t=false; }
                                                                }
                                                   }
                                                   if (q=='N'){ // NEGRAS vs blancas
                                                                if (d[f-i][c-i]==2 || d[f-i][c-i]=='B'){ d[f-i][c-i]=' ';
                                                                                                         v1[n]=2; n++;
                                                                }
                                                                if (d[f-i][c-i]==1 || d[f-i][c-i]=='N'){ cout << " *** NO PUEDE COMER FICHAS NEGRAS ***" << endl;
                                                                                                         if (t==false){ t=true; }
                                                                                                         else{ t=false; }
                                                                }
                                                   }
                                               }
                                               d[f][c]=' ';
                                               d[f-x][c-x]=q;
                                            }
                                            
                                            if (d[f-x][c-x]!=' ' && e==true){ cout << " *** NO PUEDE SALTAR HASTA ALLA ***" << endl;
                                                                             if (t==false){ t=true; }
                                                                             else{ t=false; }        
                                            }
                                  }                 
                                                                     
                                  if (c<a){ // derecha
                                            x=a-c;
                                            if (d[f-x][c+x]==' '){ 
                                               e=false;
                                               for (int i=1; i<x ; i++){                                                   
                                                   if (q=='B'){ // BLANCAS vs negras
                                                                if (d[f-i][c+i]==1 || d[f-i][c+i]=='N'){ d[f-i][c+i]=' ';
                                                                                                         v2[b]=1; b++;
                                                                }
                                                                if (d[f-i][c+i]==2 || d[f-i][c+i]=='B'){ cout << " *** NO PUEDE COMER FICHAS BLANCAS ***" << endl;
                                                                                                         if (t==false){ t=true; }
                                                                                                         else{ t=false; }
                                                                }
                                                   }
                                                   if (q=='N'){ // NEGRAS vs blancas
                                                                if (d[f-i][c+i]==2 || d[f-i][c+i]=='B'){ d[f-i][c+i]=' ';
                                                                                                         v1[n]=2; n++;
                                                                }
                                                                if (d[f-i][c+i]==1 || d[f-i][c+i]=='N'){ cout << " *** NO PUEDE COMER FICHAS NEGRAS ***" << endl;
                                                                       if (t==false){ t=true; }
                                                                       else{ t=false; }
                                                                }
                                                   }
                                               }
                                               d[f][c]=' ';
                                               d[f-x][c+x]=q;                           
                                            } 
                                            if (d[f-x][c+x]!=' ' && e==true){ cout << " *** NO PUEDE SALTAR HASTA ALLA ***" << endl;
                                                                             if (t==false){ t=true; }
                                                                             else{ t=false; }        
                                            }
                                  }
                             break;                                                            
                             
                             case 'v':
                                  if (c>a){ // izquierda
                                            x=c-a;
                                            if (d[f+x][c-x]==' '){
                                               e=false;                
                                               for (int i=1; i<x ; i++){                                                   
                                                   if (q=='B'){ // BLANCAS vs negras
                                                                if (d[f+i][c-i]==1 || d[f+i][c-i]=='N'){ d[f+i][c-i]=' ';
                                                                                                         v2[b]=1; b++;
                                                                }
                                                                if (d[f+i][c-i]==2 || d[f+i][c-i]=='B'){ cout << " *** NO PUEDE COMER FICHAS BLANCAS ***" << endl;
                                                                                                         if (t==false){ t=true; }
                                                                                                         else{ t=false; }
                                                                }
                                                   }
                                                   if (q=='N'){ // NEGRAS vs blancas
                                                                if (d[f+i][c-i]==2 || d[f+i][c-i]=='B'){ d[f+i][c-i]=' ';
                                                                                                         v1[n]=2; n++;
                                                                }
                                                                if (d[f+i][c-i]==1 || d[f+i][c-i]=='N'){ cout << " *** NO PUEDE COMER FICHAS NEGRAS ***" << endl;
                                                                                                         if (t==false){ t=true; }
                                                                                                         else{ t=false; }
                                                                }
                                                   }
                                               }               
                                               d[f][c]=' ';
                                               d[f+x][c-x]=q;                  
                                            }
                                            if (d[f+x][c-x]!=' ' && e==true){ cout << " *** NO PUEDE SALTAR HASTA ALLA ***" << endl;
                                                                             if (t==false){ t=true; }
                                                                             else{ t=false; }        
                                            }                               
                                  }
                      
                                  if (c<a){ // derecha
                                            x=a-c;                               
                                            if (d[f+x][c+x]==' '){ 
                                               e=false;
                                               for (int i=1; i<x ; i++){                                                   
                                                   if (q=='B'){ // BLANCAS vs negras
                                                                if (d[f+i][c+i]==1 || d[f+i][c+i]=='N'){ d[f+i][c+i]=' ';
                                                                                                         v2[b]=1; b++;
                                                                }
                                                                else { cout << " *** NO PUEDE COMER FICHAS BLANCAS ***" << endl;
                                                                       if (t==false){ t=true; }
                                                                       else{ t=false; }
                                                                }
                                                   }
                                                   if (q=='N'){ // NEGRAS vs blancas
                                                                if (d[f+i][c+i]==2 || d[f+i][c+i]=='B'){ d[f+i][c+i]=' ';
                                                                                                         v1[n]=2; n++;
                                                                }
                                                                else { cout << " *** NO PUEDE COMER FICHAS NEGRAS ***" << endl;
                                                                       if (t==false){ t=true; }
                                                                       else{ t=false; }
                                                                }
                                                   }
                                               }
                                               d[f][c]=' ';
                                               d[f+x][c+x]=q;                                                                 
                                            } 
                                            if (d[f+x][c+x]!=' ' && e==true){ cout << " *** NO PUEDE SALTAR HASTA ALLA ***" << endl;
                                                                             if (t==false){ t=true; }
                                                                             else{ t=false; }        
                                            }                      
                                  }
                             break;
                                  
                             default: cout << " *** MOVIMIENTO NO VALIDO use ^ o v ***" << endl;
                                      if (t==false){ t=true; }
                                      else{ t=false; }
                      }
                   }                                                                                                                                                                                                        
    }                    
    cout << endl << " "; system ("pause");
    
    // coronación
    for (c=0; c<8; c++){
                        if (d[0][c]==2){ d[0][c]='B'; } // reina BLANCA
                        if (d[7][c]==1){ d[7][c]='N'; } // reina NEGRA   
    }

    // ganador
    int j1=0, j2=0;
    for (int i=0; i<12; i++){ if (v1[i]==2){ j1++; }
                              if (v2[i]==1){ j2++; }
    }    
    if (j1==12){ system ("cls");
                cout << endl << "   " << l << " ~ JUGADOR 2 HA GANADO ~ " << l << endl;
                r=true;
                cout << endl << "   "; 
                system ("pause"); 
    }                
    if (j2==12){ system ("cls");
                cout << endl << "   " << l << " ~ JUGADOR 1 HA GANADO ~ " << l << endl;
                r=true;
                cout << endl << "   ";
                system ("pause"); 
    }
    
    // modo prueba
    if (f==23){ mp=true;    
                for (f=0; f<8; f++){
                    for (c=0; c<8; c++){                        
                        d[f][c]=' ';                    
                        if (f==2){ if (c%2==0){ d[f][c]=1; } // NEGRAS
                                   else{ d[f][c]=' '; }
                        }
                        
                        if (f==5){ if (c%2!=0){ d[f][c]=2; } // BLANCAS
                                   else{ d[f][c]=' '; }
                        }                       
                    }
                }
                d[1][3]='N'; d[6][4]='B';
                b=7; n=7;               
                for (int i=0; i<7; i++){ v1[i]=2; v2[i]=1; }
    }                    
} while (r==false); 
return 0;    
}

    
    
